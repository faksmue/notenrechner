module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',
  pwa: {
    name: 'Notenrechner',
    shortName: 'Noten',
    themeColor: '#ffffff',
    backgroundColor: '#ffffff',
    display: 'standalone',
    icons: [
      {
        src: 'android-chrome-192x192.png',
        sizes: '192x192',
        type: 'image/png'
      },
      {
        src: 'android-chrome-256x256.png',
        sizes: '256x256',
        type: 'image/png'
      },
      {
        src: 'android-chrome-512x512.png',
        sizes: '512x512',
        type: 'image/png'
      }
    ],
    manifestOptions: {
      name: 'Notenrechner',
      shortName: 'Notenrechner',
      startUrl: '.'
    }
  }
}
