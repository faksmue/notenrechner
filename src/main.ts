import Vue from 'vue'
import App from './App.vue'
import Layout from './layout'
import Ui from './ui'
import './registerServiceWorker'
import './less/main.less'
import router from './router'

Vue.config.productionTip = false
Vue.use(Layout)
Vue.use(Ui)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
