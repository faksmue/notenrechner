// install all ui components by default
import Vue from 'vue'

function install (vue: typeof Vue): void {
  // read all .vue files in the current dir
  const req = require.context('.', false, /\w+\.vue/)
  req.keys().forEach(filename => {
    const component = req(filename)
    const componentName = filename.slice(2, -4) // './xxx.vue' => 'xxx'

    // component name is identical to the filename
    vue.component(componentName, component.default || component)
  })
}

export default install
