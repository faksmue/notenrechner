import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../pages/page-home.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/sps',
    component: () => import('../pages/page-sps.vue')
  },
  {
    path: '/erzieher',
    component: () => import('../pages/page-erzieher.vue')
  },
  {
    path: '/englisch',
    component: () => import('../pages/page-englisch.vue')
  },
  {
    path: '/about',
    component: () => import('../pages/page-about.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
