function calcPruefungsnote (schriftlich: string, muendlich: string): [string, string] {
  if (schriftlich === '-') return ['-', '-']
  if (muendlich === '-') return [schriftlich, schriftlich]
  const s = parseInt(schriftlich)
  const m = parseInt(muendlich)
  const result = ((2 * s + m) / 3)
  return [result.toFixed(2), Math.round(result).toFixed(0)]
}

function candoOral (jftg: string, pruef: string): boolean {
  if (jftg === '-') return false
  if (pruef === '-') return false
  const [j, p] = [parseInt(jftg), parseInt(pruef)]
  return p > j && Math.abs(j - p) % 2 === 1
}

function calcEndnote (jahresfortgang: string, pruefung: string, muendlich: string): [string, string] {
  if (jahresfortgang === '-') return ['-', '-']
  if (pruefung === '-') return ['-', '-']
  const [j, p, m] = [parseInt(jahresfortgang), parseInt(pruefung), parseInt(muendlich)]
  if (j === 1 && p === 2 && m === 1) return ['1.5', '1']
  const result = (j + p) / 2
  if (result === Math.floor(result)) return [result.toFixed(0), result.toFixed(0)]
  if (p > j) return [result.toFixed(1), Math.floor(result + 1).toFixed(0)]
  return [result.toFixed(1), Math.floor(result).toFixed(0)]
}

function calcEndnoteEnglisch (jahresfortgang: string, pruefung: string, muendlich: string): [string, string] {
  if (pruefung === '6') return ['6', '6']
  return calcEndnote(jahresfortgang, pruefung, muendlich)
}

export {
  calcPruefungsnote,
  calcEndnoteEnglisch,
  calcEndnote,
  candoOral
}
