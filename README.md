# Notenrechner

Für Studierende der Fachakademie für Sozialpädagogik Mühldorf.

Es berechnet die Endnoten nach Jahresfortgang und schriflicher Prüfung.
In einer Tabelle ist sichtbar, welche Ergebnisse bei einer freiwilligen
mündlichen Prüfung möglich sind. Dies kann eine Hilfe sein bei der Entscheidung,
ob man eine mündliche Prüfung ablegen möchte oder nicht.

## Erreichbarkeit

Diese Projekt findet man unter

https://faksmue.gitlab.io/notenrechner

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
